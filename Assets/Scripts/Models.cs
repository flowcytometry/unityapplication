using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ident
{
    public string _id;
    public virtual string Name { get; }
}

[Serializable]
public class Patient : Ident
{
    public string firstname;
    public string lastname;
    public string email;
    public string phone;
    public string doctor;
    public int __v;
    public int num_tests;

    public override string Name
    {
        get {
            return firstname + " " + lastname;
        }
    }
}


[Serializable]
public class Position : Ident // (replace w/ Vector3?)
{
    public double z;
    public double y;
    public double x;
}

[Serializable]
public class Scale : Ident // (replace w/ Vector3?)
{
    public double z;
    public double y;
    public double x;
}

[Serializable]
public class Shape : Ident
{
    public string shape_type;
    public Position position;
    public Scale scale;
    public string color;
}

[Serializable]
public class Cluster : Ident
{
    public string name;
    public List<Shape> shapes;
    public string date_created;

    public override string Name 
    {
        get 
        {
            return name;
        }
    }
}

[Serializable]
public class Metadata : Ident
{
    public string patient;
    public string uploaded_file;
    public int __v;
    public string processed_file;
    public int num_points;
    // TODO: THIS PROPERTY IS WRONG AND NEEDS TO BE REMOVED FROM DB AND THIS FILE!! 
    // BITCH, THIS WHY YOU DONT TYPO
    public string processed_files;
    public string doctor;
    public List<Cluster> clusters;
    public int status;
    public string upload_date;

    public override string Name
    {
        get
        {
            // TODO: format date from UTC into something prettier
            return upload_date;
        }
    }
}


[Serializable]
public class MetaDataItem 
{
    public Metadata data;
}


[Serializable]
public abstract class DataList<T> {
    public List<T> data;
}

[Serializable]
public class PatientsList : DataList<Patient> { }

[Serializable]
public class MetadataList : DataList<Metadata> { }

[Serializable]
public class PointsList : DataList<Vector3> { }