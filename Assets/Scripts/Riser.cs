﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Riser : MonoBehaviour {

	private float endingY = 0;
	public float RiseSpeed = 20;
	private Renderer render;

	private Color initialColor;
	private Color endColor;

	// Use this for initialization
	void Start () {
		var pos = transform.position;
		initialColor = new Color(pos.x, 0, pos.z);
		endColor = new Color(pos.x, pos.y, pos.z);
		endingY = transform.position.y;
		transform.position = new Vector3(transform.position.x, 0, transform.position.z);
		render = GetComponent<Renderer>();
		render.material.color = endColor;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < endingY) {
			float lerp = Mathf.PingPong(Time.time, 1.0f) / 1.0f;
			transform.position += new Vector3(0, 0.01f * Time.deltaTime * RiseSpeed, 0);
			// render.material.color = Color.Lerp(initialColor, endColor, lerp);
		}
	}
}
