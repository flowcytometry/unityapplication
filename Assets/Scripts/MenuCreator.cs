﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

using Hover.Core.Items.Types;
using Hover.Core.Layouts.Arc;
using Hover.InterfaceModules.Cast;
using Hover.Core.Items;

public class MenuCreator : MonoBehaviour 
{
	public HovercastInterface hoverCast;
	public GameObject RowPrefab;
	public GameObject SelectorPrefab;
	public int RowLength = 5;
	public PointPlotter plotter;

	private List<Cluster> selectedClusterSet;

	void Awake()
	{	
		StartCoroutine(Api.FetchPatients(PopulatePatientMenu));
	}

	void PopulatePatientMenu(List<Patient> patients) 
	{
		if(patients.Count == 0) return;
			
		var rows = CreateRows(patients, group => CreateDataRow(group, OnPatientSelected, "Patient"));
		hoverCast.ActiveRow = rows[0].GetComponent<HoverLayoutArcRow>();
	}

	public void OnPatientSelected(IItemDataSelectable pItem) 
	{
		StartCoroutine(Api.FetchMetadataList(pItem.Id, (metadataList) => {
			var rows = PopulateMetadataMenu(metadataList);
			SetRowTransition(rows, pItem);
		}));
	}

	List<GameObject> PopulateMetadataMenu(List<Metadata> metadataList) 
	{
		var rows = CreateRows(metadataList, group => CreateDataRow(group, OnMetadataSelected, "Metadata"));
		return rows;
	}

	public void OnMetadataSelected(IItemDataSelectable pItem) 
	{
		plotter.ClearPoints();

		StartCoroutine(Api.FetchMetadata(pItem.Id, (metadata) => {
			var rows = PopulateClusterMenu(metadata.clusters);
			selectedClusterSet = metadata.clusters;
			SetRowTransition(rows, pItem);
			StartCoroutine(Api.FetchPoints(metadata.processed_file, (points) => {
				plotter.PlotPoints(points);
			}));		
		}));
	}

	List<GameObject> PopulateClusterMenu(List<Cluster> clusterList) 
	{
		var rows = CreateRows(clusterList, group => CreateDataRow(group, OnClusterSelected, "Cluster"));
		return rows;
	}

	private void OnClusterSelected(IItemDataSelectable pItem) 
	{
		Debug.Log("Cluster was selected!!!");
		var selectedCluster = selectedClusterSet.Find(cluster => cluster._id == pItem.Id);
		plotter.ColorPoints(selectedCluster.shapes);
	}

#region Here Be Generic Functions
	List<GameObject> CreateRows<T>(List<T> data, Func<IGrouping<int, T>, GameObject> CreateRow) {
		var itemsPerRow = RowLength - 1;

		// Make groups of items, corresponding to rows
		var dataGroups = data.Select((item, i) => new { Item = item, Index = i })
							 .GroupBy(p => p.Index/itemsPerRow, p => p.Item);
		
		// Create the rows from the data groups, and add the next buttons to each row
		var rows = dataGroups.Select<IGrouping<int, T>, GameObject>(CreateRow).ToList();
		AddNextButtons(rows);
		
		// Add the rows to the hand menu
		foreach(var row in rows) {
			row.transform.parent = hoverCast.RowContainer;
			row.transform.localPosition = Vector3.zero;
			row.transform.localRotation = Quaternion.identity;
		}

		return rows;
	}

	GameObject CreateDataRow<T>(IGrouping<int, T> dataGroup, ItemEvents.SelectedHandler OnSelected, string rowName) where T : Ident {
		var row = Instantiate(RowPrefab, Vector3.zero, Quaternion.identity);

		foreach(var data in dataGroup) {
			var dataSelector = Instantiate(SelectorPrefab, Vector3.zero, Quaternion.identity);
			var selector = dataSelector.GetComponent<HoverItemDataSelector>();
			dataSelector.name = data.Name;
			selector.Id = data._id;
			selector.Label = data.Name; 
			selector.OnSelected += OnSelected;
			dataSelector.transform.parent = row.transform;
		}

		row.name = rowName + dataGroup.Key;
		return row;
	}

	void AddNextButtons(List<GameObject> rows) {
		for(int i = 0; i < rows.Count(); i++) {
			var currentRow = rows[i];

			if ( i > 0 ) {
				Debug.Log("Set " + i + " to false");
				currentRow.gameObject.SetActive(false);
			}

			if( i == rows.Count() - 1 )
				return;
				
			var nextRow = rows[i+1];
			var nextButton = Instantiate(SelectorPrefab);
			var selector = nextButton.GetComponent<HoverItemDataSelector>();
			var transitionInfo = nextButton.GetComponent<HovercastRowSwitchingInfo>();
			var nextRowObj = nextRow.GetComponent<HoverLayoutArcRow>();

			nextButton.name = "Next";
			nextButton.transform.parent = currentRow.transform;
			nextButton.GetComponent<HoverItemDataSelector>().OnSelected += hoverCast.OnRowSwitched;
			selector.Id = "Next-" + i;
			selector.Label = "Next";
			transitionInfo.NavigateToRow = nextRowObj;

		}
	}

	void SetRowTransition(List<GameObject> rows, IItemDataSelectable pItem) {
		if (rows.Count == 0) return;

		var transitionInfo = pItem.gameObject.GetComponent<HovercastRowSwitchingInfo>();
		var transitionItem = rows[0].GetComponent<HoverLayoutArcRow>();
		transitionInfo.NavigateToRow = transitionItem;
		hoverCast.OnRowSwitched(pItem);				
	}
#endregion
}