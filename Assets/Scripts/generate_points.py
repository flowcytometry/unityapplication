"""Generate a random number of points into a file"""

import json
from random import randint
from pprint import pprint

def rand_point():
	return {
		"x": randint(5000, 100000),
		"y": randint(5000, 100000),
		"z": randint(5000, 100000)
	}

def main():
	points = [rand_point() for i in range(randint(1000, 5000))]
	points_obj = {
		"data": points
	}

	with open("../Data/points.json", "w") as points_file:
		json.dump(points_obj, points_file)

if __name__ == '__main__':
	main()