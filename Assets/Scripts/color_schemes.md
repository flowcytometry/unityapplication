Color Schemes
=============

#00027F
#4C4FFF
#0003FF
#26277F
#0003CC

https://color.adobe.com/create/color-wheel/?base=2&rule=Monochromatic&selected=0&name=My%20Color%20Theme&mode=hex&rgbvalues=0,0.00626770418898559,0.5,0.30000000000000004,0.30877478586457985,1,0,0.01253540837797118,1,0.15000000000000002,0.15438739293228992,0.5,0,0.010028326702376945,0.8&swatchOrder=0,1,2,3,4