﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Auth : MonoBehaviour {
	public InputField email;
	public InputField password;
	private string loginURL = "http://192.168.0.17:3001/api/login";

	public string authCookie;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
	}

	public void Login() {
		StartCoroutine(Login(email.text, password.text));
	}

	private IEnumerator Login(string email, string password) {
		WWWForm loginForm = new WWWForm();
		loginForm.AddField("email", "dsmalley@email.com");
		loginForm.AddField("password", "0000");

		WWW res = new WWW(loginURL, loginForm);

		yield return res;
		yield return !string.IsNullOrEmpty(res.error);

		authCookie = res.responseHeaders["SET-COOKIE"];
		UnityEngine.SceneManagement.SceneManager.LoadScene("Graph");
		Debug.Log("end of login");
	}

	public void ClearToken() {
		authCookie = "";
	}
}
