using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class CreateShape : MonoBehaviour {
    private Controller controller;
    private bool makingShape = false;
    public Transform shape;


	// Use this for initialization
	void Start () {
        controller = new Controller();
	}
	
	// Update is called once per frame
	void Update () {
        var frame = controller.Frame();
        var hands = frame.Hands;
        //Debug.Log("Hands: " + hands.Count);
        if(hands.Count > 0 && hands[0].PinchStrength > 0.7)
        {
            var fingers = hands[0].Fingers;
            var fingerLoc = fingers[0].TipPosition;
            var vec = new Vector3(fingerLoc.x, fingerLoc.y, fingerLoc.z);
            shape.position = vec;
        }
	}
}
