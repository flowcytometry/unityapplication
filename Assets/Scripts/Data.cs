﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.Networking;

namespace GAPO {
	public class Data : MonoBehaviour {
		public static string api = "http://192.168.0.3:3001/api";
		private static string authCookie = "";
		public static List<Vector3> pointsFromApi; 

		/// <summary>
		/// Start is called on the frame when a script is enabled just before
		/// any of the Update methods is called the first time.
		/// </summary>
		void Start()
		{
			authCookie = GameObject.FindGameObjectWithTag("auth").GetComponent<Auth>().authCookie;
            Debug.Log("abuot to call load pats");
            StartCoroutine(LoadPatients());
		}
        
		private IEnumerator LoadPatients() {
            Debug.Log("inside load pats");
			var url = $"{api}/patients";
			var headers = new Dictionary<string, string>();
				headers.Add("Cookie", authCookie);
			WWW www = new WWW(url, null, headers);

			// Wait for resposne
			yield return www;
            var patients = JsonUtility.FromJson<PatientsList>(www.text);

		}
		public static List<Vector3> LoadPointsFile(string filename = "./Assets/Data/points.json") {
			string points_txt = File.ReadAllText(filename);
			var json = JsonUtility.FromJson<PointsList>(points_txt);
			return json.data;
		}

		/** Gets the processed points from the web api, converts them from json to 
		  * an array of Vector3 values, and sets pointsFromApi to the result.
		  */
		public IEnumerator LoadPointsFromApi(string email = "", string password = "") {
			// Login if there is no authentication cookie present
			if(string.IsNullOrEmpty(authCookie)) {
				// Wait for login to finish
				yield return StartCoroutine(Login(email, password));
			}

			if(string.IsNullOrEmpty(authCookie)) {
				Debug.Log("Could not log in.");
				yield return null;
			} else {	
				// TODO(xavier): Fix this from being a magic number.
				var points_location = $"{api}/processed/2/download";
				var headers = new Dictionary<string, string>();
				headers.Add("Cookie", authCookie);
				WWW www = new WWW(points_location, null, headers);

				// Wait for the response
				yield return www;

				var json = JsonUtility.FromJson<PointsList>(www.text);
				pointsFromApi = json.data;
			}
		}

		public static IEnumerator Login(string email, string password) {
			string loginURL = $"{api}/login";
			WWWForm loginForm = new WWWForm();

			loginForm.AddField("email", email);
			loginForm.AddField("password", password);

			WWW loginResponse = new WWW(loginURL, loginForm);

			// Return if there is an error?
			yield return !string.IsNullOrEmpty(loginResponse.error);

			authCookie = loginResponse.responseHeaders["SET-COOKIE"];
			yield return null;
		}
	}
}
