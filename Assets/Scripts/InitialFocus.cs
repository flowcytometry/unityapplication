﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialFocus : MonoBehaviour {
	public Transform focusObject;
	// Use this for initialization
	void Start () {
		transform.LookAt(focusObject.position, -Vector3.up);
	}
}
