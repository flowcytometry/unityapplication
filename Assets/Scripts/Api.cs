using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Api {
	public static string api = "http://192.168.0.17:3001/api";
	private static string authCookie = GameObject.FindGameObjectWithTag("auth").GetComponent<Auth>().authCookie;

	public static IEnumerator APIFetch(string url, Action<string> callback) {
		var headers = new Dictionary<string, string>();
		headers.Add("Cookie", authCookie);
		WWW www = new WWW(url, null, headers);
		yield return www;
		callback(www.text);
	}

    public static IEnumerator FetchDataList<T, U> (string url, Action<List<T>> callback) where U : DataList<T> {
        url = $"{api}/{url}";
        var data = new List<T>();

        yield return APIFetch(url, (response) => {
            var fromJson = JsonUtility.FromJson<U>(response);
			data = fromJson.data;
        });
		Debug.Log(data[0]);
        callback(data);
    }

	public static IEnumerator FetchPatients(Action<List<Patient>> callback) {
		yield return FetchDataList<Patient, PatientsList>("patients", callback);
	}

    public static IEnumerator FetchMetadataList(string patientId, Action<List<Metadata>> callback) {
        yield return FetchDataList<Metadata, MetadataList>($"metadata/patient/{patientId}", callback);
    }

    public static IEnumerator FetchMetadata(string metadataId, Action<Metadata> callback) {
		Metadata data = null;

		yield return APIFetch($"{api}/metadata/{metadataId}", (response) => {
			Debug.Log(response);
            data = JsonUtility.FromJson<MetaDataItem>(response).data;
        });

		callback(data);
    }

	public static IEnumerator FetchPoints(string processedId, Action<List<Vector3>> callback) {
		yield return FetchDataList<Vector3, PointsList>($"processed/{processedId}/download", callback);
	}
}