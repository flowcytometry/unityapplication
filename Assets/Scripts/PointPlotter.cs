﻿using System.Collections.Generic;
using UnityEngine;


public class PointPlotter : MonoBehaviour 
{
	public Transform PointPrefab;
	public Transform CenterPoint;
	public Vector3 sizeScale = new Vector3(0.01f, 0.01f, 0.01f);
	public Vector3 locationScale = new Vector3(.00001f, 0.00001f, 0.00001f);

	public Vector3 StartScale = new Vector3(0.5f, 0.5f, 0.5f);
	public Vector3 EndScale = new Vector3(2f, 2f, 2f);

	public void PlotPoints(List<Vector3> points) 
	{
		
		Vector3 center = Vector3.zero;
		
		float largest_x = float.NegativeInfinity;
		float largest_y = float.NegativeInfinity;
		float largest_z = float.NegativeInfinity;

		float smallest_x = float.PositiveInfinity;
		float smallest_y = float.PositiveInfinity;
		float smallest_z = float.PositiveInfinity;
		
		foreach(var point in points) {
			largest_x = Mathf.Max(point.x, largest_x);
			largest_y = Mathf.Max(point.y, largest_y);
			largest_z = Mathf.Max(point.z, largest_z);

			smallest_x = Mathf.Min(point.x, smallest_x);
			smallest_y = Mathf.Min(point.y, smallest_y);
			smallest_z = Mathf.Min(point.z, smallest_z);
		}

		var xBound = largest_x - smallest_x;
		var yBound = largest_y - smallest_y;
		var zBound = largest_z - smallest_z;
		
		foreach(var point in points) {
			float x = (point.x - smallest_x) / xBound;
			float y = (point.y - smallest_y) / yBound;
			float z = (point.z - smallest_z) / zBound;
			
			var location = new Vector3( x, y, z ) * 5;
			center += location;
			var pt = Instantiate(PointPrefab, location, Quaternion.identity);
			pt.parent = transform;
			pt.tag="point";
		}

		// center = center / points.Length;
	}

	public void ClearPoints() 
	{
		var points = GameObject.FindGameObjectsWithTag("point");
		foreach(var point in points) {
			Destroy(point);
		}
	}

	public void ColorPoints(List<Shape> shapes) 
	{
		shapes.Reverse();
		var points = GameObject.FindGameObjectsWithTag("point");
		foreach(var point in points) {
			var spriteRender = point.gameObject.GetComponent<SpriteRenderer>();
			spriteRender.color = GetColor(point.transform.position, shapes);
		}
	}

	private Color GetColor(Vector3 point, List<Shape> shapes)
	{
		Color color;

		foreach(var shape in shapes)  {
			ColorUtility.TryParseHtmlString(shape.color, out color);

			if( shape.shape_type == "box" && InCube(point, shape) ) {
				color = hexToColor(shape.color);
				return color;
			} else if( shape.shape_type == "sphere" && InSphere(point, shape) ) {
				color = hexToColor(shape.color);
				return color;
			}
		}
		
		ColorUtility.TryParseHtmlString("#90a4ae", out color);
		return hexToColor("#90a4ae");		
	}

	public Color hexToColor(string hex)
	{
         hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
         hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
         byte a = 255;//assume fully visible unless specified in hex
         byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
         byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
         byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
         //Only use alpha if the string has enough characters
         if(hex.Length == 8){
             a = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
         }
         return new Color32(r,g,b,a);
	}

	private bool InCube(Vector3 point, Shape cube)
	{
		var pos_x_check = cube.position.x + (cube.scale.x / 2);
		var neg_x_check = cube.position.x - (cube.scale.x / 2);

		var pos_y_check = cube.position.y + (cube.scale.y / 2);
		var neg_y_check = cube.position.y - (cube.scale.y / 2);

		var pos_z_check = cube.position.z + (cube.scale.z / 2);
		var neg_z_check = cube.position.z - (cube.scale.z / 2);

		return point.x <= pos_x_check && point.x >= neg_x_check &&
				point.y <= pos_y_check && point.y >= neg_y_check &&
				point.z <= pos_z_check && point.z >= neg_z_check;
	}

	private bool InSphere(Vector3 point, Shape sphere)
	{
		float x_displacement = (float) (point.x - sphere.position.x);
		float y_displacement = (float) (point.y - sphere.position.y);
		float z_displacement = (float) (point.z - sphere.position.z);

		var radius = (float) sphere.scale.x;
		var distance = Mathf.Sqrt((x_displacement*x_displacement)+(y_displacement*y_displacement)+(z_displacement*z_displacement));

		return distance <= radius;
	}
}

